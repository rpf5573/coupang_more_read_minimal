<?php
/**
 * Plugin Name: Coupang More Read Ad
 * Plugin URI: https://kmong.com/gig/155175
 * Description: 쿠팡 더보기 광고 플러그인
 * Version: 1.0
 * Author: WPMate
 * Author URI: https://kmong.com/gig/155175
 */

if (!defined('ABSPATH'))
  exit;

// require 'vendor/autoload.php';

require "vendor/meta-box/meta-box/meta-box.php";
require "vendor/meta-box/mb-settings-page/mb-settings-page.php";
require "vendor/meta-box/meta-box-aio/meta-box-aio.php";
require "vendor/meta-box/meta-box-tooltip/meta-box-tooltip.php";

$plugin_slug = 'coupang_more_read_ad';
$option_name = $plugin_slug;

function custom_wptalk_wp_auto_ads_show_after_click_plugin_custom_script()
{
  wp_enqueue_script('jquery');
  wp_enqueue_script('jquery-cookie-plugin', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js', array(), null, true);

  wp_enqueue_style('wp-auto-ads-show-after-click', plugin_dir_url(__FILE__) . 'assets/css/main.css');
}
add_action('wp_enqueue_scripts', 'custom_wptalk_wp_auto_ads_show_after_click_plugin_custom_script');

add_filter('mb_settings_pages', function ($settings_pages) {
  global $option_name;

  $settings_pages[] = [
    'id' => $option_name,
    'option_name' => $option_name,
    'menu_title' => '쿠팡 더보기 광고 설정',
    'parent' => 'tools.php',
  ];
  return $settings_pages;
});

add_filter('rwmb_meta_boxes', function ($meta_boxes) {
  global $option_name;

  $meta_boxes[] = [
    'id' => $option_name . '_options',
    'title' => '쿠팡 더보기 광고 설정',
    'context' => 'normal',
    'settings_pages' => $option_name,
    'tab' => 'general',
    'fields' => [
      [
        'name' => '쿠키 만료 시간 (분단위)',
        'id' => 'cookie_expire',
        'type' => 'number',
        'min' => 0,
        'step' => 1,
        'std' => 120,
        'label_description' => '쿠키 만료 시간을 설정합니다.',
        'tooltip' => '예를들어 120을 입력하면,  후에 다시 광고가 보입니다'
      ],
      [
        'name' => '카운트다운 딜레이 (초단위)',
        'id' => 'countdown_delay',
        'type' => 'number',
        'min' => 0,
        'step' => 1,
        'std' => 2,
        'label_description' => '카운트다운 딜레이 시간을 설정합니다.',
        'tooltip' => '예를들어 2을 입력하면, 2초후에 카운트다운이 시작됩니다'
      ],
      [
        'name' => '카운트다운 (초단위)',
        'id' => 'countdown',
        'type' => 'number',
        'min' => 0,
        'step' => 1,
        'std' => 5,
        'label_description' => '카운트다운 시간을 설정합니다.',
        'tooltip' => '예를들어 5을 입력하면, 5초 후에 X버튼이 나옵니다',
      ],
      [
        'name' => '쿠팡 광고 iframe',
        'id' => 'ad_iframe_text',
        'type' => 'textarea',
        'label_description' => "https://partners.coupang.com 에서 <br> 생성한 iframe을 입력합니다",
        'sanitize_callback' => 'none',
      ],
    ],
  ];
  return $meta_boxes;
});

add_filter('the_content', function ($content) {
  global $option_name;

  // post 상세 페이지에서만 작동한다
  if (!is_singular('post')) {
    return $content;
  }
  $post_id = get_the_ID();

  $cookie_expire = rwmb_get_value('cookie_expire', ['object_type' => 'setting'], $option_name) ?: 120;
  $countdown_delay = rwmb_get_value('countdown_delay', ['object_type' => 'setting'], $option_name) ?: 2;
  $countdown = rwmb_get_value('countdown', ['object_type' => 'setting'], $option_name) ?: 5;
  $coupang_iframe = rwmb_get_value('ad_iframe_text', ['object_type' => 'setting'], $option_name) ?: '';

  ob_start(); ?>
  <div id="coupang-more-read-js-data" data-cookie_expire="<?php echo $cookie_expire; ?>"
    data-countdown_delay="<?php echo $countdown_delay; ?>" data-countdown="<?php echo $countdown; ?>"></div>
  <div class="continue_coupang" data-post-id="<?php echo $post_id; ?>" data-role="btnMoreReadDynamic" style="">
    <div class="coupang_wrap">
      <p class="title">
        <span>인기 상품</span> 확인하고 계속 읽어보세요!
      </p>
      <div class="cp_content_dynamic" id="cp_dynamic_ad">
        <div class="d_flex a_center cp_skip_simple">
          <p class="more_cnt">5</p>
          <button data-post-id="<?php echo $post_id; ?>" type="button" class="more_read"><i></i></button>
        </div>
        <?php echo $coupang_iframe; ?>
      </div>
      <div class="d_flex txt_back character_888">
        <span>원치 않을 경우 뒤로가기를 눌러주세요</span>
      </div>
    </div>
  </div>
  <?php
  $new_content = ob_get_clean() . $content;
  return $new_content;
}, 999999999999);

add_action('wp_footer', function () {

  // post 상세 페이지에서만 작동한다
  if (!is_singular('post')) {
    return;
  } ?>

  <script type="text/javascript">
    (($) => {
      // 이거는 한개밖에 없다 어차피
      const coupang_ad = $('.continue_coupang');
      if (coupang_ad.length === 0) return;

      const post_id = $(coupang_ad).data('post-id');
      if (!post_id) return;

      const cookie_key = `coupang_load_more_ad_${post_id}`;
      const $post = $('.single-post .has-post-thumbnail');
      if ($post === null) return;

      $(document).ready(() => {
        const checkCookie = $.cookie(cookie_key);
        const $button = $('.more_read');
        const $data = $('#coupang-more-read-js-data');
        const cookie_expire = Number($data.data('cookie_expire'));
        const countdown_delay = Number($data.data('countdown_delay'));
        const countdown = Number($data.data('countdown'));

        // 쿠키가 있으면 뭐 할게 없어
        if (checkCookie) {
          $('body').addClass('full-view');
          return;
        }

        // 쿠키가 없으면 CSS는 그대로 두고 이벤트만 건다
        const openFullView = () => {
          // 글을 보여주고
          $('body').addClass('full-view');

          // 쿠키를 만들어서 120분 동안 보이지 않게 한다
          const expDate = new Date();
          expDate.setTime(expDate.getTime() + (cookie_expire * 60 * 1000));
          $.cookie(cookie_key, 'value', { expires: expDate, path: '/' });
        };

        $button.on('click', openFullView);

        // 쿠팡 광고를 클릭했을 때도 X버튼을 클릭했을때와 동일한 로직을 적용하자
        document.addEventListener('visibilitychange', () => {
          if (!document.hidden) {
            openFullView();
          }
        });

        let count = countdown;

        const $counter = $($post.find('.more_cnt'));
        $counter.text(count);

        const showXButton = () => {
          $($post.find('.more_read')).addClass('show');
        };
        const hideCountButton = () => {
          $counter.addClass('hidden');
        }

        // 카운트다운 딜레이
        setTimeout(() => {
          // 카운터 시작
          const intervalTimer = setInterval(() => {
            count -= 1;
            $counter.text(count);
            if (count <= 0) {
              hideCountButton();
              requestAnimationFrame(() => {
                showXButton();
              });
              clearInterval(intervalTimer);
            }
          }, 1000);
        }, countdown_delay * 1000);
      });
    })(jQuery);
  </script>
  <?php

}, 999999);
